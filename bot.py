import logging
from logging import info, error, exception, warning
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', filename='dev.log')

info('Loading dependencies...')
from telegram import Update, ReplyKeyboardMarkup, ReplyKeyboardRemove
from telegram.ext import Updater, Defaults, CallbackContext, CommandHandler, MessageHandler, ConversationHandler, Filters
# from models import User, Person
import schedule
import threading
import time
from datetime import date
import schedule
import threading


class BirthdayBot:
    def __init__(self):
        info('Creating Bot instance...')
        self.bot = Updater('1576028058:AAHJiYGYqEdS76RGU2SWUy5Jy15-adehhG8')
        
        self.dispatcher = self.bot.dispatcher

        self.dispatcher.add_handler(CommandHandler('start', self.Start))
        self.dispatcher.add_handler(CommandHandler('help', self.Help))

        self.dispatcher.add_handler(MessageHandler(Filters.text('Кванториум лучше всех!'), self.EasterEgg))

        self.scheduler = threading.Thread(target=self.StartScheduling)
        self.scheduler.start()
    
    def Start(self, update:Update, context:CallbackContext):
        info(f'Got start message from { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        update.message.reply_text(
            '*Привет\!*\n\n'
            '`Я могу напомнить тебе о днях рождения твоих друзей\!`',
            parse_mode='MarkdownV2',
        )
    
    def Help(self, update:Update, context:CallbackContext):
        info(f'Got start message from { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        update.message.reply_text(
            '*Заблудился?*\n\n'
            '`Я могу напомнить тебе о днях рождения твоих друзей\!`\n'
            '`Для этого просто сделай то-то`\n',
            parse_mode='MarkdownV2',
        )
    
    def EasterEgg(self, update:Update, context:CallbackContext):
        info(f'Got start message from { update.message.from_user.first_name } { update.message.from_user.last_name } ({ update.message.from_user.id })')
        update.message.reply_text(
            '*Пасхалка\!*\n\n',
            parse_mode='MarkdownV2',
        )
        update.message.reply_photo(
            photo='https://upload.wikimedia.org/wikipedia/commons/thumb/5/54/Bg-easter-eggs.jpg/1200px-Bg-easter-eggs.jpg'
        )

    def NotifyUsers(self):
        info('Performing user notification task...')
        # for user in User.select():
        #     for person in Person.select().where(Person.user == user):
        #         print(user, '  -  ', person)
    
    def StartScheduling(self):
        info('Initializing scheduler...')
        schedule.every().day.at('10:00:00').do(self.NotifyUsers)
        while True:
            schedule.run_pending()
            time.sleep(1)

    def run(self):
        info('Starting bot...')
        self.bot.start_polling()
        self.bot.idle()


if __name__ == "__main__":
    bot = BirthdayBot()
    bot.run()